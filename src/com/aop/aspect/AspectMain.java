
package com.aop.aspect;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * 类MainApp.java的实现描述：TODO 类实现描述 
 * @author kewei.songkw 2013-5-9 下午2:29:42
 */
public class AspectMain {

    /**
     * @param args
     */
    @SuppressWarnings("resource")
    public static void main(String[] args) {      
        ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");        
        Student student = (Student) context.getBean("student");        
        student.getAge();
        student.getName();
        student.printThrowException();
    }

}
