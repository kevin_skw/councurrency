
package com.aop.aspect;

/**
 * 类Student.java的实现描述：TODO 类实现描述 
 * @author kewei.songkw 2013-5-9 下午2:25:13
 */
public class Student {

   private String name;
   
   private String age;


/**
 * @return the name
 */
public String getName() {
    System.out.println("Name:"+name);
    return name;
}


/**
 * @param name the name to set
 */
public void setName(String name) {
    this.name = name;
}


/**
 * @return the age
 */
public String getAge() {
    System.out.println("Age:"+age);
    return age;
}


/**
 * @param age the age to set
 */
public void setAge(String age) {
    this.age = age;
}
   

public void printThrowException(){
    System.out.println("Exception raised");
    throw new IllegalArgumentException();
}
   

}
