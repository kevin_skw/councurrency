package com.aop.aspect;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

/**
 * 类Logging.java的实现描述：TODO 类实现描述
 * 
 * @author kewei.songkw 2013-5-9 下午2:12:37
 */
@Aspect
public class Logging {

    /**
     * Following is the definition for a pointcut to select all the methods available. So advice will be called for all
     * the methods.
     */
    @Pointcut("execution(* com.aop.aspect.*.*(..))")
    public void selectAll() {
    };

    /**
     * Following is the definition for a pointcut to select getter methods available. So advice will just be called for
     * getter methods.
     */
    @Pointcut("execution(* com.aop.aspect.Student.get*(..))")
    public void selectGet() {
    };

    /**
     * This is the method which I would like to execute before a selected method execution.
     */
    @Before("selectAll()")
    public void beforeAdvice() {
        System.out.println("Going to setup student profile.");
    }

    /**
     * This is the method which I would like to execute after a selected method execution.
     */
    @After("selectGet()")
    public void afterAdvice() {
        System.out.println("Student profile has been setup");
    }

    /**
     * This is the method which I would like to execute when any method returns.
     */
    @AfterReturning(pointcut="selectGet()",returning="retVal")
    public void afterReturningAdvice(Object retVal) {
        System.out.println("Returning:" + retVal.toString());
    }

    /**
     * This is the method which I would like to execute if there is an exception raised.
     */
    @AfterThrowing(pointcut="selectAll()",throwing="ex")
    public void afterThrowingAdvice(IllegalArgumentException ex) {
        System.out.println("There has been an exception:" + ex.toString());
    }

}
