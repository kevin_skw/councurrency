
package com.aop.spring;

/**
 * 类Logging.java的实现描述：TODO 类实现描述
 * 
 * @author kewei.songkw 2013-5-9 下午2:12:37
 */
public class Logging {

    /**
     * This is the method which I would like to execute before a selected method execution.
     */
    public void beforeAdvice() {
        System.out.println("Going to setup student profile.");
    }

    /**
     * This is the method which I would like to execute after a selected method execution.
     */
    public void afterAdvice() {
        System.out.println("Student profile has been setup");
    }

    /**
     * This is the method which I would like to execute when any method returns.
     */
    public void afterReturningAdvice(Object retVal) {
        System.out.println("Returning:" + retVal.toString());
    }

    /**
     * This is the method which I would like to execute if there is an exception raised.
     */
    public void afterThrowingAdvice(IllegalArgumentException ex) {
        System.out.println("There has been an exception:" + ex.toString());
    }

}
