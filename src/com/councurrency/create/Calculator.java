package com.councurrency.create;

/**
 * 类Calculator.java的实现描述：TODO 类实现描述 
 * @author kewei.songkw 2013-5-8 下午7:54:37
 */
public class Calculator implements Runnable {
    
    private int number;
    
    
    /**
     * @param number
     */
    public Calculator(int number){
        super();
        this.number = number;
    }

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
       for(int index=0;index<10;index++){
           System.out.printf("%s:%d*%d=%d\n",Thread.currentThread().getName(),number,index,index*number);
       }
    }

}
